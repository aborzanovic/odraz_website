
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import { routes } from './routes'
import App from './components/App.vue'
import Navigation from './components/Navigation.vue'
import locales from './i18n'

Vue.use(VueI18n)

// set lang
Vue.config.lang = 'sr'

// set locales
Object.keys(locales).forEach(function (lang) {
  Vue.locale(lang, locales[lang])
})

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes
})

Vue.component('app', App)
Vue.component('navigation', Navigation)

const app = new Vue({
    el: '#app',
    router
})
