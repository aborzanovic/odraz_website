export default {
	navigation: {
		sustainable_development: {
			_: "sustainable development",
			what: "what is sustainable development",
			why: "why sustainable development",
			experience: "experience of developed countries",
			environmental_protection: {
				_: "environmental protection",
				current: "current problem with environmental protection in the region",
				law: "law and guidebooks from the area of environmental protection"
			},
			agriculture: {
				_: "agriculture",
				current: "current events in agriculture in the region",
				law: "law and guidebooks from the area of agriculture"
			},
			industry: {
				_: "industry",
				current: "current events in industry of Pančevo",
				law: "law and guidebooks from the area of industry"
			},
			fire_protection: {
				_: "fire protection",
				current: "current events in the area of fire protection in Pančevo",
				law: "law and guidebooks from the area of fire protection"
			}
		},
		projects: {
			_: "projects",
			current: "current projects",
			finished: "finished projects",
			project_management: {
				_: "project management",
				point: "the point of project management",
				metodology: "metodology",
				licence: "licence system",
				examples: "examples of project management",
				gallery: "gallery"
			}
		},
		education: {
			_: "education",
			schedule: "schedule of planned lectures",
			archive: "lectures archive"
		},
		news: "news",
		about_us: {
			_: "about us",
			vision: "vision",
			mission: "mission",
			goals: "goals and tasks",
			contact: "contact"
		},
		blog: "blog",
		support: "support"
	},
	global: {
		link: "link | links",
		download: "download"
	}
}