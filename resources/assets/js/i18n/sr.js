export default {
	navigation: {
		sustainable_development: {
			_: "održivi razvoj",
			what: "šta je održivi ravoj",
			why: "zašto održivi razvoj",
			experience: "iskustva industrijski razvijenih zemalja",
			environmental_protection: {
				_: "zaštita životne sredine",
				current: "aktuelni problem u zaštiti životne sredine u regionu",
				law: "zakoni i pravilnici iz oblasti zaštite životne sredine"
			},
			agriculture: {
				_: "poljoprivreda",
				current: "događaji u oblasti poljoprivrede u  regionu",
				law: "zakoni i pravilnici iz oblasti poljoprivrede"
			},
			industry: {
				_: "industrija",
				current: "aktuelna događanja  u Pančevačkoj industriji",
				law: "zakoni i pravilnici iz oblasti industrije"
			},
			fire_protection: {
				_: "zaštita od požara",
				current: "aktuelna događanja u Pančevu u oblasti zaštite od požara",
				law: "zakoni i pravilnici iz oblasti zaštite od požara"
			}
		},
		projects: {
			_: "projekti",
			current: "aktuelni projekti",
			finished: "završeni projekti",
			project_management: {
				_: "upravljanje projektima",
				point: "svrha upravljanja projektima",
				metodology: "metodologija",
				licence: "sistem licenci",
				examples: "primeri upravljanja projektima",
				gallery: "galerija"
			}
		},
		education: {
			_: "edukacija",
			schedule: "raspored planiranih predavnja",
			archive: "arhiva predavanja"
		},
		news: "vesti",
		about_us: {
			_: "o nama",
			vision: "vizija",
			mission: "misija",
			goals: "ciljevi i zadaci",
			contact: "kontakt"
		},
		blog: "blog",
		support: "podrška"
	},
	global: {
		link: "link | linkovi",
		download: "preuzimanje"
	}
}