import Home from './components/Home.vue';

export const routes = [
	{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/what-is-sustainable-development',
		name: 'sustainable_development.what',
		component: Home
	},
	{
		path: '/why-sustainable-development',
		name: 'sustainable_development.why',
		component: Home
	},
	{
		path: '/experience-of-developed-countries',
		name: 'sustainable_development.experience',
		component: Home
	},
	{
		path: '/current-problem-with-environment',
		name: 'sustainable_development.environmental_protection.current',
		component: Home
	},
	{
		path: '/environment-laws-and-guidebooks',
		name: 'sustainable_development.environmental_protection.law',
		component: Home
	},
	{
		path: '/environment-links',
		name: 'sustainable_development.environmental_protection.links',
		component: Home
	},
	{
		path: '/current-events-in-agriculture',
		name: 'sustainable_development.agriculture.current',
		component: Home
	},
	{
		path: '/agriculture-laws-and-guidebooks',
		name: 'sustainable_development.agriculture.law',
		component: Home
	},
	{
		path: '/agriculture-links',
		name: 'sustainable_development.agriculture.links',
		component: Home
	},
	{
		path: '/current-events-in-industry',
		name: 'sustainable_development.industry.current',
		component: Home
	},
	{
		path: '/industry-laws-and-guidebooks',
		name: 'sustainable_development.industry.law',
		component: Home
	},
	{
		path: '/industry-links',
		name: 'sustainable_development.industry.links',
		component: Home
	},
	{
		path: '/current-events-in-fire-protection',
		name: 'sustainable_development.fire_protection',
		component: Home
	},
	{
		path: '/fire-protection-laws-and-guidebooks',
		name: 'sustainable_development.fire_protection.law',
		component: Home
	},
	{
		path: '/fire-protection-links',
		name: 'sustainable_development.fire_protection.links',
		component: Home
	},
	{
		path: '/current-projects',
		name: 'projects.current',
		component: Home
	},
	{
		path: '/finished-projects',
		name: 'projects.finished',
		component: Home
	},
	{
		path: '/point-of-project-management',
		name: 'projects.project_management.point',
		component: Home
	},
	{
		path: '/project-management-metodology',
		name: 'projects.project_management.metodology',
		component: Home
	},
	{
		path: '/project-management-licence-system',
		name: 'projects.project_management.licence',
		component: Home
	},
	{
		path: '/project-management-examples',
		name: 'projects.project_management.examples',
		component: Home
	},
	{
		path: '/project-management-gallery',
		name: 'projects.project_management.gallery',
		component: Home
	},
	{
		path: '/project-management-links',
		name: 'projects.project_management.links',
		component: Home
	},
	{
		path: '/education-schedule',
		name: 'education.schedule',
		component: Home
	},
	{
		path: '/education-archive',
		name: 'education.archive',
		component: Home
	},
	{
		path: '/news',
		name: 'news',
		component: Home
	},
	{
		path: '/about-us-vision',
		name: 'about_us.vision',
		component: Home
	},
	{
		path: '/about-us-mission',
		name: 'about_us.mission',
		component: Home
	},
	{
		path: '/about-us-goals',
		name: 'about_us.goals',
		component: Home
	},
	{
		path: '/about-us-contact',
		name: 'about_us.contact',
		component: Home
	},
	{
		path: '/blog',
		name: 'blog',
		component: Home
	},
	{
		path: '/support',
		name: 'support',
		component: Home
	}
]